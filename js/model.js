var Person = Backbone.Model.extend(
{
	defaults:
	{
		name: 'John',
		age: 12,
		address: 'China Henan Shangcai......'
	},
	validate: function( attributes )
	{
		if( attributes.name === undefined )
		{
			return 'human must have name ..fool...';
		}
	},
	initialize: function()
	{

		this.on('change', function()
		{
			console.log('something is changed!!!');
		});

		this.on('change', function()
		{
			console.log('the second function invoked!!!');
		});

		this.on('change:src', function()
		{
			console.log('src change please carefully');
		});

		this.on('invalid', function( model, error )
		{
			console.log(model, error);
		});

		console.log('I am initialize!!!');
	}
});

var PersonView = Backbone.View.extend(
{
	className: 'person-view-class',
	id: 'person-view-id',
	tagName: 'li',

	initialize: function()
	{
		this.render();
	},

	template: _.template('<strong><%= name %> </strong>+ (<%= age %>) -- <%=address%>'),

	render: function()
	{
		this.$el.html(this.template(this.model.toJSON()));
		$(document.body).append(this.$el);
	}
});

var PersonCollection = Backbone.Collection.extend(
{
	model: Person
});

var person1 = new Person(
{
	name: 'hhstuhacker',
	age: 12,
	address: 'beijing'
});

var person2 = new Person(
{
	name: '杨路光',
	age: 25,
	address: 'HeNan ShangCai......'
});

var personCollection = new PersonCollection();

personCollection.add(person1);
personCollection.add(person2);

var personCollection2 = new PersonCollection(
[
    {
	    name: 'hhstuhacker',
	    age: 12,
	    address: 'beijing'
    },
    {
	    name: '杨路光',
	    age: 25,
	    address: 'HeNan ShangCai......'
    }
]);

